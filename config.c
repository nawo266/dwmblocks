#include "config.h"

#include "block.h"
#include "util.h"

// clang-format off
Block blocks[] = {
// 	*command* 		*update interval* 	*update signal*
// 				* in seconds *
	{"song",			3,			21},
	{"updates-arch",		7200,			20},
	{"news",			3600,			19},
	{"weather",			3600,			18},
	{"cpu",				10,			17},
	{"memory",			30,			16},
	{"battery",			30,			15},
	{"volume",			0,			10},
	{"clock",			60,			1},

};
// clang-format on

const unsigned short blockCount = LEN(blocks);
